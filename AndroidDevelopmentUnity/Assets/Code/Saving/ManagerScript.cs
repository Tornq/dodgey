using UnityEngine;

public class ManagerScript : MonoBehaviour
{

    public static ManagerScript Instance { get; private set; }
    public static int Counter { get; private set; }

    // Use this for initialization
    void Start()
    {
        Instance = this;
    }

    public void IncrementCounter()
    {
        Counter++;
    }

    public void RestartGame()
    {
        PlayGamesScript.AddScoreToLeaderboard(GPGSIds.leaderboard_global, Counter);

        /*if (Counter > CloudVariables.Highscore)
        {
            CloudVariables.Highscore = Counter;
            PlayGamesScript.Instance.SaveData();
        }*/

        Counter = 0;
    }

}