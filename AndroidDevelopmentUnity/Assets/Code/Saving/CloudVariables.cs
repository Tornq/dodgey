using UnityEngine;
using SaveSystem.DTOs;

public class CloudVariables : MonoBehaviour
{
    //public static int Highscore { get; set; }

    public static ScoreDataDto score { get; set; }
    public static CurrencyDto currency { get; set; }
    public static BuyableDto buyable { get; set; }

}