﻿using GameFlowSystem.View;
using GooglePlayGames;
using UnityEngine;
using PlayerSystem;
using ScoreSystem;
using CosmeticSystem;
using ShopSystem;

public class AchievementHandler : MonoBehaviour
{
    GameObject menu;
    MainMenuUI main;

    //References to other scripts
    [SerializeField] private HealthComponent health;
    [SerializeField] private ScoreManager score;
    [SerializeField] private CosmeticController cosmetic;
    [SerializeField] private ShopController shop;

    //Bools to set the achievement as done or not
    [SerializeField] private bool one, two, three, four, five, six, seven, eight, nine;

    // Start is called before the first frame update
    void Start()
    {
        //Finds the Main menu panel and gets the component Main menu ui
        menu = GameObject.Find("MainMenuPanel");
        main = menu.GetComponent<MainMenuUI>();

        //Open Game achievement
        Social.ReportProgress("CgkI2NuiwMsUEAIQAg", 100.0f, (success) =>
        {
            Debug.Log("Opened Game");
           // handle success or failure
        });
    }

    // Update is called once per frame
    void Update()
    {
        //Surviving without loosing health
        //30sec
        if (Time.deltaTime >= 0.3 && main.hasBeenStarted == true && health.CurrentHealth == 3 && one == false)
        {
            one = true;
            AchievementOne();
        }
        //60sec
        if (Time.deltaTime >= 0.6 && main.hasBeenStarted == true && health.CurrentHealth == 3 && two == false)
        {
            two = true;
            AchievemntTwo();
        }
        //120sec
        if (Time.deltaTime >= 1.2 && main.hasBeenStarted == true && health.CurrentHealth == 3 && three == false)
        {
            three = true;
            AchievemtThree();
            
        }

        //Surviving 3min
        if (Time.deltaTime >= 1.8 && main.hasBeenStarted == true && four == false)
        {
            four = true;
            AchievemtFour();
        }

        //Dodged objects
        //Dodged one
        if(score.CurrentScore == 1 && five == false)
        {
            five = true;
            AchievementFive();

        }
        //Dodged 10
        if (score.CurrentScore == 10 && six == false)
        {
            six = true;
            AchievementSix();
        }

        //Skins
        //Equipt a skin
        if(cosmetic.CurrentSkin != null && seven == false)
        {
            seven = true;
            AchievementSeven();
        }
        //Buy a skin
        if(shop._unlockedAchievement == true && eight == false)
        {
            eight = true;
            AchievementEigth();
        }

        //Started a playthrough
        if(main.hasBeenStarted == true && nine == false)
        {
            nine = true;
            AchievementNine();
        }
    }

    //Unlocks Achievement one
    private void AchievementOne()
    {
        // unlock achievement
        Social.ReportProgress("CgkI2NuiwMsUEAIQBg", 100.0f, (success) =>
        {
          // handle success or failure
          Debug.Log("Novice");
         });
    }
    //Unlocks Achievement two
    private void AchievemntTwo()
    {
        Social.ReportProgress("CgkI2NuiwMsUEAIQBw", 100.0f, (success) =>
        {
            Debug.Log("Expert");
            // handle success or failure
        });
    }
    //Unlocks Achievement three
    private void AchievemtThree()
    {
        Social.ReportProgress("CgkI2NuiwMsUEAIQCA", 100.0f, (success) =>
        {
            Debug.Log("Master");
            // handle success or failure
        });
    }
    //Unlocks Achievement four
    private void AchievemtFour()
    {
        Social.ReportProgress("CgkI2NuiwMsUEAIQBQ", 100.0f, (success) =>
        {
            Debug.Log("Survived");
            // handle success or failure
        });
    }
    //Unlocks Achievement five
    private void AchievementFive()
    {
        Social.ReportProgress("CgkI2NuiwMsUEAIQAw", 100.0f, (success) =>
        {
                Debug.Log("first Object");
                // handle success or failure
        });

    }
    //Unlocks Achievement six
    private void AchievementSix()
    {
        Social.ReportProgress("CgkI2NuiwMsUEAIQBA", 100.0f, (success) =>
        {
            Debug.Log("10 Object");
            // handle success or failure
        });
    }
    //Unlocks Achievement seven
    private void AchievementSeven()
    {
        Social.ReportProgress("CgkI2NuiwMsUEAIQCg", 100.0f, (success) =>
        {
            Debug.Log("Equipt");
            // handle success or failure
        });
    }
    //Unlocks Achievement eight
    private void AchievementEigth()
    {

        Social.ReportProgress("CgkI2NuiwMsUEAIQCQ", 100.0f, (success) =>
        {
            Debug.Log("Skin");
            // handle success or failure
        });
    }
    //Unlocks Achievement nine
    private void AchievementNine()
    {
        Social.ReportProgress("CgkI2NuiwMsUEAIQCw", 100.0f, (success) =>
        {
            Debug.Log("PLAY");
            // handle success or failure
        });
    }
}