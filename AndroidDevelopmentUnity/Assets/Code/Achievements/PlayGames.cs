using UnityEngine;
using System;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using ScoreSystem;
using GooglePlayGames.BasicApi.SavedGame; 

//Class for the Play games Leaderboard
public class PlayGames : MonoBehaviour
{
    //ID to the leaderboard
    string leaderboardID = "CgkI2NuiwMsUEAIQAA";
    //Reference to the play games platform
    public static PlayGamesPlatform platform;
    //Reference to the score manager
    [SerializeField] private ScoreManager score;

    void Start()
    {
        //If the pgp is null, we set it up
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            platform = PlayGamesPlatform.Activate();
        }
        //Authenticate the user
        Social.Active.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Logged in successfully");
            }
            else
            {
                Debug.Log("Login Failed");
            }
        });
    }
    //Fuction to add score to the leaderboard
    public void AddScoreToLeaderboard()
    {
        if (Social.Active.localUser.authenticated)
        {
            Social.ReportScore(score.Score, leaderboardID, success => { });
        }
    }
    //Function to display the gp leaderboard
    public void ShowLeaderboard()
    {
        if (Social.Active.localUser.authenticated)
        {
            platform.ShowLeaderboardUI();
        }
    }

  
}