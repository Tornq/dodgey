using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SaveSystem; 

public class SavingInstance : MonoBehaviour
{
    //reference to savedata script
    public SaveData saveData = new SaveData(); 

    //Enable Saved games feature
    private void Awake()
    {
        saveData.EnableSavedGames(); 
    }

    //Function called from button to open UI
    public void OpenSavedGamesUI()
    {
        saveData.OpenUI(); 
    }
}
