﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using SaveSystem.DTOs;
using SaveSystem.Interfaces;
using UnityEngine;

namespace SaveSystem
{
	/// <summary>
	/// 	Class representing the actual save data of the game.
	/// </summary>
	[Serializable]
	public class SaveData
	{
		#region Serialize Fields

		[SerializeField] private ScoreDataDto _scoreData = new ScoreDataDto();
		[SerializeField] private CurrencyDto _currencyData = new CurrencyDto();
		[SerializeField] private BuyableDto _buyableData = new BuyableDto();

		#endregion

		#region Private Fields

		private Dictionary<string, ISaveData> _save = new Dictionary<string, ISaveData>();
        private uint maxNumToDisplay;
        private bool allowCreateNew;
        private bool allowDelete;
        TimeSpan totalTimePlayed { get; }
        byte[] _bytes;

        #endregion

        #region Constructors


        public void EnableSavedGames()
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            // enables saving game progress.​
            .EnableSavedGames()
            .Build();
            PlayGamesPlatform.InitializeInstance(config);
        }

        public void OpenUI()
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.ShowSelectSavedGameUI("Select saved game",
              maxNumToDisplay,
              allowCreateNew,
              allowDelete,
              OnSavedGameSelected);
        }

        private void OnSavedGameSelected(SelectUIStatus arg1, ISavedGameMetadata arg2)
        {
            OpenSavedGame("");
        }

  


        void OpenSavedGame(string filename)
        {
           ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.OpenWithAutomaticConflictResolution(
              filename,
              DataSource.ReadCacheOrNetwork,
              ConflictResolutionStrategy.UseLongestPlaytime,
              OnSavedGameOpened
              );
        }

        public void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
        {

            if (status == SavedGameRequestStatus.Success)
            {
                // handle reading or writing of saved game.
                SaveGame(game, _bytes, totalTimePlayed);
            }
            else
            {
                // handle error
                Debug.Log("FUCK");
            }
        }

        void SaveGame(ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

            SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
            builder = builder
                .WithUpdatedPlayedTime(totalPlaytime)
                .WithUpdatedDescription("Saved game at " + DateTime.Now);

            /*if (savedImage != null)
            {
                // savedImage is an instance of Texture2D​
                // call getScreenshot() to set savedImage​
                byte[] pngData = savedImage.EncodeToPNG();
                builder = builder.WithUpdatedPngCoverImage(pngData);
            }
            */

            SavedGameMetadataUpdate updatedMetadata = builder.Build();
            savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
        }

        public void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            if (status == SavedGameRequestStatus.Success)
            { 
                // handle reading or writing of saved game.​
            }
            else
            {
                // handle error​
            }
        }

        /*public Texture2D getScreenshot()
        {
            // Create a 2D texture that is 1024x700 pixels from which the PNG will be​
            // extracted​
            Texture2D screenShot = new Texture2D(1024, 700);
            // Takes the screenshot from top left hand corner of screen and maps to top​
            // left hand corner of screenShot texture​
            screenShot.ReadPixels(new Rect(0, 0, Screen.width, (Screen.width/1024)*700), 0, 0);
            return screenShot;
        }
        */

        // <summary>​
        ///    write the save game as .json to disk.​
        /// </summary>​
        /// <param name="path">Path where the save game is being stored.</param>​
        /// <param name="saveData">Actual save data</param>​
        public void Serialize(string path, SaveData saveData)
        {
            string serializedJSON = JsonUtility.ToJson(saveData);
            File.WriteAllText(path, serializedJSON);
        }

        public void JsonToBinary()
        {
            string json = "{Score:10}";
            byte[] bytes = Encoding.ASCII.GetBytes(json);
            _bytes = bytes;
        }

        public void BinaryToJson(byte[] bytes)
        {
            string str = Encoding.ASCII.GetString(bytes);
        }

        void LoadGameData(ISavedGameMetadata game)
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
        }

        public void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                // handle processing the byte array data​
            }
            else
            {
                // handle error​

            }
        }

        void DeleteGameData(string filename)
        {
            // Open the file to get the metadata.​
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.OpenWithAutomaticConflictResolution(
                filename,
                DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime,
                DeleteSavedGame);
        }

        public void DeleteSavedGame(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
                savedGameClient.Delete(game);
            }
            else
            {
                // handle error​
            }
        }



        public SaveData()
		{

            Initialize();
		}

		#endregion

		#region Public methods

		/// <summary>
		/// 	Reinitialize the save game.
		/// </summary>
		public void Initialize()
		{
			_save = new Dictionary<string, ISaveData>
			        {
				        { SaveKeys.ScoreDataKey, _scoreData },
				        { SaveKeys.CurrencyDataKey, _currencyData },
				        { SaveKeys.BuyableDataKey, _buyableData },
			        };
		}

		/// <summary>
		/// 	Updates the data for the given key in the save game.
		/// </summary>
		/// <param name="key">Key to update</param>
		/// <param name="saveData">Save Data object</param>
		/// <typeparam name="T">Data type of updated save data</typeparam>
		public void Put<T>(string key, T saveData) where T : ISaveData
		{
			if (_save.ContainsKey(key))
			{
				_save[key].Save(saveData);
			}
			else
			{
				Debug.LogError("Trying to save data for a non-existing key!");
			}
		}

		/// <summary>
		/// 	Gets the save data for the given key in the save game.
		/// </summary>
		/// <param name="key">Key to update</param>
		/// <param name="saveData">Save Data object</param>
		/// <typeparam name="T">Data type of updated save data</typeparam>
		/// <returns>A <see cref="ISaveData"/> object containing the save data or a default if there is none.</returns>
		public ISaveData Get<T>(string key, T defaultValue) where T : ISaveData
		{
			if (_save.ContainsKey(key))
			{
				return _save[key];
			}

			Debug.LogWarning("Trying to load data for a non-existing key!");
			return defaultValue;
		}

		#endregion
	}
}