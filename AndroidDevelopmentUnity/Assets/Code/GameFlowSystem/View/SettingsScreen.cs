using GooglePlayGames;
using System.Collections;
using System.Collections.Generic;
using Core.Extensions; 
using UnityEngine;

public class SettingsScreen : MonoBehaviour
{
    //Reference to canvas group of settings panel
    private CanvasGroup _canvasGroup;

    //Disable settings at the beginning
    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.Disable(); 
    }

    //Enable settings screen
    public void Show()
    {
        _canvasGroup.Enable(); 
    }

    //Disable settings screen
    public void Close()
    {
        _canvasGroup.Disable(); 
    }

}
