﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameFlowSystem.View
{
	public class MainMenuUI : MonoBehaviour
	{
		#region Static Stuff

		public static event Action OnStartGame;
		public static event Action OnOpenShop;
        public static event Action OnOpenSettings; 

		#endregion

		#region Serialize Fields

		[SerializeField] private Button _startGameButton;
		[SerializeField] private Button _shopButton;
        [SerializeField] private Button _settingsButton;
        public bool hasBeenStarted;

        [SerializeField] private SettingsScreen _settingsscreen;
        #endregion

        #region Unity methods

        private void Awake()
		{
			_startGameButton.onClick.AddListener(OnClickStartGame);
			_shopButton.onClick.AddListener(OnClickShop);
            _settingsButton.onClick.AddListener(OnClickOpenSettings);
        }

		#endregion

		#region Private methods

		private void OnClickShop()
		{
			OnOpenShop?.Invoke();
		}

		private void OnClickStartGame()
		{
			OnStartGame?.Invoke();
            hasBeenStarted = true;

            //Close settings screen when starting game 
            _settingsscreen.Close();
        }

        private void OnClickOpenSettings()
        {
            OnOpenSettings?.Invoke();
        }

		#endregion
	}
}