﻿using Core;
using GameFlowSystem.View;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class SettingsController : Singleton<SettingsController>
{
    //Variables for the button and screen references 
    [SerializeField] private Button _signOutButton;
    [SerializeField] private Button _signInButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private SettingsScreen _screen; 
    

    protected override void Awake()
    {
        base.Awake();

        //sub event
        MainMenuUI.OnOpenSettings += OpenSettings; 

        //Sign in and sign out button listeners
        _signOutButton.onClick.AddListener(OnClickSignOutButton);
        _signInButton.onClick.AddListener(OnClickSignInButton);

        // back button listener
        _backButton.onClick.AddListener(OnClickBackButton);
    }

    private void OnDestroy()
    {
        // unsub all of the above
        MainMenuUI.OnOpenSettings -= OpenSettings;

        _backButton.onClick.RemoveAllListeners();
    }

    //Open Settings
    private void OpenSettings()
    {
        _screen.Show(); 
    }

    //hides settings window
    private void CloseSettings()
    {
        _screen.Close();
    }

    //lets player sign out from play store
    private void SignOut()
    {
        Debug.Log("Signing Out");
        PlayGamesPlatform.Instance.SignOut();
    }

    //lets player sign into play store
    private void SignIn()
    {
        //Code for Authentication 
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        // authenticate user:​
        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce,
        (result) =>
        {
                 /*switch (result)
                 {
                     case SignInStatus.Success:
                         break;
                     default:
                         break; 
                 }
                 */
        });
        
    }

    //close settings
    private void OnClickBackButton()
    {
        CloseSettings();
    }

    //Sign out 
    private void OnClickSignOutButton()
    {
        SignOut();
    }

    //Sign in
    private void OnClickSignInButton()
    {
        SignIn();
    }
}
