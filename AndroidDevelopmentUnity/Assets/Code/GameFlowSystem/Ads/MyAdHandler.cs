﻿using CurrencySystem;
using CurrencySystem.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class MyAdHandler : MonoBehaviour, IUnityAdsListener
{
    //Setting ad unit id
    public string MyPlacementId = "Rewarded_Android";

    //Setting GameID and test mode
    private const string GameId = "4202493";
    private const bool IsTestMode = true;

    //Bool to check if ad was watched
    private bool wasWatched;

    //References
    [SerializeField] private Button _adButton;
    [SerializeField] private CoinPurse coinpurseScript;

    private void Start()
    {
        // Set wasWatched to false
        wasWatched = false;

        //Subscribe to event
        Advertisement.AddListener(this);

        //Initialze ads SDK
        Advertisement.Initialize(GameId, IsTestMode);
        _adButton.enabled = false;
    }

    //Unsubsribe from event
    private void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }

    //Run ad
    public void ShowRewardedAd()
    {
        Advertisement.Show(MyPlacementId);
    }

    //Enable button when ad is ready
    public void OnUnityAdsReady(string placementId)
    {
        // enable button
        if( wasWatched == false)
        {
            _adButton.enabled = true;
        }
    }

    //Debug warning message when ads did error
    public void OnUnityAdsDidError(string message)
    {
        // log the message​
        Debug.LogWarning(message);
    }

    //Freeze time when ad plays
    public void OnUnityAdsDidStart(string placementId)
    {
        Time.timeScale = 0;
    }

    //React when ad finished playing 
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        //Don´t reward when ad didn´t finish
        if (!placementId.Equals(MyPlacementId))
        {
            return;
        }
        
        //When ad finished playing reward player
        if (showResult == ShowResult.Finished)
        {
            Reward(50, CurrencyType.Regular);
        }
    }

    //reward by adding extra coins
    private void Reward(int amount, CurrencyType type)
    {
        Debug.Log("50 Coins");
        coinpurseScript.AddCoinsToTotal(amount);
        wasWatched = true;
        _adButton.enabled = false;
    }
}

